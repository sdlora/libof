LIB=		of
SRCS=		libof.c of10.c utils.c
HDRS=		libof.h of10.h
SHLIB_MAJOR=	0
SHLIB_MINOR=	1
#MAN=		libof.3
#MLINKS=		libof.3 of_controller_init.3

CFLAGS+=	-Wall -Werror -I.
COPTS+=		-g
LDADD+=		-lhashtab


includes:
	@cd ${.CURDIR}; for i in $(HDRS); do \
	    j="cmp -s $$i ${DESTDIR}/usr/include/$$i || \
		${INSTALL} ${INSTALL_COPY} -o ${BINOWN} -g ${BINGRP} \
		-m 444 $$i ${DESTDIR}/usr/include"; \
	    echo $$j; \
	    eval "$$j"; \
	done

.include <bsd.lib.mk>
